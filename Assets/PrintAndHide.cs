﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class PrintAndHide : MonoBehaviour
{
    // Start is called before the first frame update
    int counter = 0;
    public Renderer rend;
    public GameObject Red;
    public GameObject Blue;
    int number01;
    

    void Start()
    {
        number01 = Random.Range(200, 251);
        Red.SetActive(true);
        Blue.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(gameObject.name + ":" + counter);
        

        if (counter == 100)
        {
            Red.SetActive(false);
        }

        if (counter == number01)
        {
            Blue.SetActive(false);
        }

        counter++;
    }
}
